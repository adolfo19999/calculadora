
Feature: Sumar numeros en calculadora google


  Scenario Outline: Yo como usuario necesito sumar dos numeros
    Given Yo abro el navegador y busco google
    And Escribo calculadora
    When Presiono sobre boton buscar
    Given sumo <arg0>
    And clic en el signo "<arg3>"
    And sumo <arg1>
    When presiono igual
    Then muestra resultado esperado = <arg02>
    Examples:
      | arg0 | arg1 |arg3 | arg02|
      | 9    | 5    | -   | 4   |
      | 5    | 4    | +   | 9    |
      | 3    | 7    | ×   | 21   |
