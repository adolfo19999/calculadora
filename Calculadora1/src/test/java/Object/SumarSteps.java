package Object;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class SumarSteps {
    WebDriver driver;
    @Given("Yo abro el navegador y busco google")
    public void yoAbroElNavegadorYBuscoGoogle () {
        driver = new FirefoxDriver();
        driver.get("https://google.cl");
    }

    @And("Escribo calculadora")
    public void escriboCalculadora () {

        driver.findElement(By.xpath("//input[@title='Buscar']")).sendKeys("calculadora");

    }

    @When("Presiono sobre boton buscar")
    public void precionoSobreBotonBuscar () {
        driver.findElement(By.xpath("//input[@title='Buscar']")).sendKeys(Keys.ENTER);
    }


    @Given("sumo {int}")
    public void sumo(int arg0) throws InterruptedException {
        Thread.sleep(2000);
        driver.findElement(By.xpath("//div[@role='button'][contains(.,'"+arg0+"')]")).click();
    }

    @And("clic en el signo +")
    public void clicEnElSigno() throws InterruptedException {
        Thread.sleep(2000);
        driver.findElement(By.xpath("//div[@jsname='XSr6wc'][contains(.,'+')]")).click();


    }
    @And("clic en el signo {string}")
    public void clicEnElSigno(String arg3) throws InterruptedException {
        Thread.sleep(2000);
        if (arg3.compareToIgnoreCase("-") == 0){
            driver.findElement(By.xpath("//  div[@role='button'][contains(.,'−')][@aria-label='menos']")).click();}

        else {
            driver.findElement(By.xpath("//div[@role='button'][contains(.,'"+arg3+"')]")).click();}
    }



    @When("presiono igual")
    public void presionoIgual () {
        driver.findElement(By.xpath("//div[@jsname='Pt8tGc'][contains(.,'=')]")).click();

    }

    @Then("muestra resultado esperado = {int}")
    public void muestraResultadoEsperado ( int arg0){
        WebElement element = driver.findElement(By.xpath("//span[@id='cwos']"));
        Assert.assertEquals(String.valueOf(arg0), element.getText());
        driver.close();
    }
}
